FROM centos:centos6
MAINTAINER <ak.hisashi@gmail.com>

ARG RUBY_VERSION="2.5.3"

# タイムゾーンの変更
RUN cp /usr/share/zoneinfo/Asia/Tokyo /etc/localtime
COPY clock.txt /etc/sysconfig/clock
RUN chmod 0644 /etc/sysconfig/clock

# Perform updates
RUN yum -y update; yum clean all
RUN yum -y install \
openssl-devel \
readline-devel \
gcc \
gcc-c++ \
kernel-devel \
make \
wget \
ping \
sudo \
git \
tar \
ImageMagick-devel \
libxml2 \
libxslt \
libxml2-devel \
libxslt-devel

# yarn
RUN curl --silent --location https://rpm.nodesource.com/setup_6.x | bash -
RUN wget https://dl.yarnpkg.com/rpm/yarn.repo -O /etc/yum.repos.d/yarn.repo
RUN yum -y install yarn

# Rubyのインストール
RUN wget https://cache.ruby-lang.org/pub/ruby/ruby-$RUBY_VERSION.tar.gz && \
tar xvf ruby-$RUBY_VERSION.tar.gz && \
cd ruby-$RUBY_VERSION && \
./configure && \
make && \
make install && \
gem install bundler

# PostgreSQL9.6のインストール
RUN rpm -ivh https://download.postgresql.org/pub/repos/yum/9.6/redhat/rhel-6-x86_64/pgdg-centos96-9.6-3.noarch.rpm
RUN yum -y install \
postgresql96 \
postgresql96-contrib \
postgresql96-devel
